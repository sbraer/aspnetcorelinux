# Example from AZ
FROM microsoft/dotnet:1.0.1-sdk-projectjson
MAINTAINER "AZ"
COPY ["./src/MVC5ForLinuxTest2/", "/app"]
COPY ["./start.sh", "."]
RUN chmod +x ./start.sh
CMD ["./start.sh"]
