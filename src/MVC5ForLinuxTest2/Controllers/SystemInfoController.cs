﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MVC5ForLinuxTest2.DTOObjects;

namespace MVC5ForLinuxTest2.Controllers
{
    [Route("api/[controller]")]
    public class SystemInfoController : Controller
    {
        ISystemInfo _systemInfo;
        public SystemInfoController(ISystemInfo systemInfo)
        {
            _systemInfo = systemInfo;
        }
        // GET: api/values
        [HttpGet]
        public IEnumerable<DTOSystemInfo> Get()
        {
            var obj = new DTOSystemInfo();
            obj.Guid = _systemInfo.Guid;

            return new DTOSystemInfo[] {
                obj
            };
        }
    }
}
