﻿using System;

namespace MVC5ForLinuxTest2.Helpers
{
    public class SystemInfo : ISystemInfo
    {
        private Guid _guid;
        public SystemInfo()
        {
            _guid = Guid.NewGuid();
        }
        public Guid Guid
        {
            get
            {
                return _guid;
            }
        }
    }
}
