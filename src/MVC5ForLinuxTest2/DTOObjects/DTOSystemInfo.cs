﻿using System;

namespace MVC5ForLinuxTest2.DTOObjects
{
    public class DTOSystemInfo
    {
        public Guid Guid { get; set; }
    }
}
