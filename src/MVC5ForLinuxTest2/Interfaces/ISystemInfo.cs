﻿using System;

namespace MVC5ForLinuxTest2
{
    public interface ISystemInfo
    {
        Guid Guid { get; }
    }
}
